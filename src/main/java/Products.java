public enum Products {

    PSZENICA (5),
    ŻYTO (4),
    MIĘSO (20),
    TRUSKAWKI (7),
    MARCHEWKI (5),
    OGÓRKI (5),
    ARBUZY (10),
    POMIDORY (5),
    SALATA (5),
    KAPUSTA (9);


    private int price;

    Products(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
