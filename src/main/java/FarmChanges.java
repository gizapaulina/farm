public enum FarmChanges {
    MLEKO (20000),
    JAJKA (50000),
    SER (10000);

    private int value;

    FarmChanges(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
